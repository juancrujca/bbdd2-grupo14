# Trabajo de referencia UNLP – BBDD2 Cursada 2021

## Grupo 14

### Crear base de datos, usuario y permisos

Ejecutar `./createDatabase.sh`. Se pedirá la contraseña de root de la base de datos.

### Correr tests

Ejecutar `mvn clean install`
